# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models





class Apartments(models.Model):
    id = models.BigAutoField(primary_key=True)
    code = models.CharField(max_length=5, blank=True, null=True)
    floor = models.IntegerField(blank=True, null=True)
    coefficient = models.FloatField(blank=True, null=True)
    blocks_id = models.BigIntegerField()
    cant_habitant = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'apartments'


class ApartmentsHasUsers(models.Model):
    apartments_id = models.ForeignKey('Apartments', models.DO_NOTHING , db_column='apartments_id')
    users_id = models.BigIntegerField()
    ownership = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'apartments_has_users'


class Blocks(models.Model):
    id = models.BigAutoField(primary_key=True)
    apartment_amount = models.SmallIntegerField(blank=True, null=True)
    floor_amount = models.IntegerField(blank=True, null=True)
    indicative = models.CharField(max_length=5)
    condominiums_id = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'blocks'


class Categories(models.Model):
    name = models.CharField(unique=True, max_length=100)
    type = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'categories'


class Condominiums(models.Model):
    address = models.CharField(max_length=255, blank=True, null=True)
    block_amount = models.IntegerField(blank=True, null=True)
    name = models.CharField(unique=True, max_length=255)
    phone_number = models.CharField(max_length=25, blank=True, null=True)
    phone_number2 = models.CharField(max_length=25, blank=True, null=True)
    prefix = models.CharField(max_length=3)
    id_legal_representative = models.CharField(max_length=12, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'condominiums'


class DetailsCategory(models.Model):
    name = models.CharField(unique=True, max_length=100)
    sub_category = models.ForeignKey('SubCategory', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'details_category'


class Pqr(models.Model):
    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField()
    answered_at = models.DateTimeField(blank=True, null=True)
    case_number = models.CharField(max_length=11)
    description = models.CharField(max_length=255, blank=True, null=True)
    status = models.IntegerField()
    subject = models.CharField(max_length=25, blank=True, null=True)
    apartments_id = models.BigIntegerField()
    apartmentbully = models.BigIntegerField(db_column='apartmentBully', blank=True, null=True)  # Field name made lowercase.
    picture = models.TextField(blank=True, null=True)
    users_id = models.BigIntegerField()
    category = models.IntegerField(blank=True, null=True)
    sub_category = models.IntegerField(blank=True, null=True)
    det_category = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pqr'


class PqrResponse(models.Model):
    response = models.CharField(max_length=500)
    file_path = models.CharField(max_length=200, blank=True, null=True)
    pqr = models.ForeignKey(Pqr, models.DO_NOTHING)
    users = models.ForeignKey('Users', models.DO_NOTHING)
    created_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'pqr_response'

# Mapea los campos de la tabla 'Pregunta' 
class Pregunta(models.Model):
    idpregunta = models.BigAutoField(primary_key=True)
    estado = models.IntegerField(blank=True, default='1')
    pregunta = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pregunta'

# Mapea los campos de la tabla 'PreguntaRespuesta' 
class PreguntaRespuesta(models.Model):
    id = models.BigAutoField(primary_key=True)
    apartamento_id = models.IntegerField()
    #pregunta_id = models.BigIntegerField(blank=True, null=True)
    pregunta_id = models.ForeignKey('Pregunta', models.DO_NOTHING , db_column='pregunta_id')
    respuesta = models.IntegerField()
    users_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'pregunta_respuesta'


class SubCategory(models.Model):
    name = models.CharField(unique=True, max_length=100)
    category = models.ForeignKey(Categories, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'sub_category'


class UserHasCondominiums(models.Model):
    users = models.ForeignKey('Users', models.DO_NOTHING)
    condominiums = models.ForeignKey(Condominiums, models.DO_NOTHING)
    is_admin = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'user_has_condominiums'


class Users(models.Model):
    username = models.CharField(unique=True, max_length=12)
    document_type = models.CharField(max_length=2)
    email = models.CharField(max_length=750, blank=True, null=True)
    name = models.CharField(max_length=755)
    last_name = models.CharField(max_length=255)
    status = models.IntegerField()
    type = models.IntegerField()
    last_last_name = models.CharField(max_length=45, blank=True, null=True)
    last_login = models.DateTimeField(blank=True, null=True)
    password = models.CharField(max_length=200)
    phone = models.CharField(max_length=250, blank=True, null=True)
    token = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'
