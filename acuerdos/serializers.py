#se importa la libreria para usar la funciona de serializar la informacion
from rest_framework import serializers 
#importamos los modelos que vamos a usar en el serializador
from .models import (PreguntaRespuesta, Pregunta,ApartmentsHasUsers)

#se crea una clase serializer de tipo mi modelo
class PreguntaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pregunta #llamamos la clase mapeada en el archivo models.py
        fields = '__all__' #le estamos indicado que nos traiga todos las columna que corresponde al modelo Pregunta

#se crea una clase serializer de tipo mi modelo
class PreguntaRespuestaSerializer(serializers.ModelSerializer):
    class Meta:
        model = PreguntaRespuesta #llamamos la clase mapeada en el archivo models.py
        fields = [
        'id',
        'apartamento_id',
        'pregunta_id',
        'respuesta',
        'users_id'
        ] 

class ApartmentsHasUsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApartmentsHasUsers
        fields = '__all__'
        depth = 1

class ReportePreguntasSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = PreguntaRespuesta
        fields = '__all__'
        depth = 1
        
