#importamos vistas genericas y filtros para aplicar en la url
from rest_framework import viewsets, filters
from django.db.models import Count

#importamos el modelo
from .models import (PreguntaRespuesta, Pregunta ,ApartmentsHasUsers)
#importamos los serializadores creados anteriormente
from rest_framework.decorators import action
from rest_framework.response import Response
from .serializers import (PreguntaSerializer, PreguntaRespuestaSerializer,ApartmentsHasUsersSerializer,ReportePreguntasSerializer)

class PreguntaViewSet(viewsets.ModelViewSet):
    serializer_class = PreguntaSerializer #serializamos la clase 
    queryset = Pregunta.objects.all() # le indicamos que traiga todos los datos 
    filter_backends = [filters.SearchFilter] #tipo de filtro
    search_fields = ['estado'] # valor a filtra por peticio get url
    #http_method_names = ['get','post'] # limitamos la vista generica
  
class PreguntaRespuestaViewSet(viewsets.ModelViewSet):
    serializer_class = PreguntaRespuestaSerializer #serializamos la clase 
    queryset = PreguntaRespuesta.objects.all() # le indicamos que traiga todos los datos 
    filter_backends = [filters.SearchFilter]
    search_fields = ['=users_id']

class ApartmentsHasUsersViewSet(viewsets.ModelViewSet):
    queryset = ApartmentsHasUsers.objects.all()
    serializer_class = ApartmentsHasUsersSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['=users_id']
    http_method_names = ['get']

class ReportePreguntasViewset(viewsets.ModelViewSet):
    queryset = PreguntaRespuesta.objects.all()
    serializer_class = ReportePreguntasSerializer
    http_method_names = ['get']
    filter_backends = [filters.SearchFilter,filters.OrderingFilter]
    search_fields = ['=pregunta_id__idpregunta']
    ordering_fields = ['pregunta_id__idpregunta']
    @action(detail=False)
    def respuestas(self,request):
        q = self.get_queryset().values('pregunta_id__pregunta','respuesta').annotate(count=Count('respuesta')).order_by('pregunta_id')
        #pk = self.kwargs.get('pk')
        return Response(list(q))
        #.filter(pregunta_id=pregunta_id)

