"""jccloud URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
#libreria de rutas para django admin
from django.conf.urls import url
#importamos libreria para manejar rutas por defaul
from rest_framework.routers import DefaultRouter 
#importamos las viws generadas anteriomente
from acuerdos.views import PreguntaViewSet,PreguntaRespuestaViewSet,ApartmentsHasUsersViewSet,ReportePreguntasViewset
#pagina admin por defecto de django
from django.contrib import admin
#path de las url 
from django.urls import path
#definimos a que es igual router
router = DefaultRouter()
#a continuacion llamamos el viewset y le indicamos una url
#el me creara por defecto url get,post,update,delete, get/pk
#si no se limito el metodo
router.register(r'preguntas', PreguntaViewSet) 
router.register(r'preguntarespuesta', PreguntaRespuestaViewSet) 
router.register(r'usuarioxapartamento',ApartmentsHasUsersViewSet)
router.register(r'reportepreguntas',ReportePreguntasViewset),



urlpatterns = router.urls
urlpatterns += [
    
    path('admin/', admin.site.urls),
    #
]
